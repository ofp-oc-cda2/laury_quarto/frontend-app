# Be creative - Besançon

Front-end de l'application communiquant avec la partie [back-end](https://gitlab.com/ofp-oc-cda2/laury_quarto/backend_app) par le biais de l'API.
Développement réalisé en Vue.js avec l'aide de la librairie BootstrapVue.

Utilisation de Capacitor pour la version hybride de l'application mobile.

## Installation du projet en local
Utiliser le docker du dossier [backend_app](https://gitlab.com/ofp-oc-cda2/laury_quarto/backend_app) afin de faire tourner l'application complète (front et back).