import { createRouter, createWebHistory } from 'vue-router';
import Home from '@/views/Home.vue';
import Profil from '@/views/utilisateur/Profil.vue';
import Inscription from '@/views/utilisateur/Inscription.vue';
import Produits from '@/views/produit/ProfilProduits.vue';
import Connexion from '@/views/utilisateur/Connexion.vue';
import Profilvendeur from '@/views/utilisateur/Profilvendeur.vue';
import Editproduit from '@/views/produit/Editproduit.vue';
import ListingproduitCat from '@/views/produit/Catlistproduit.vue';
import Listingproduit from '@/views/produit/Listproduit.vue';
import Detailsproduit from '@/views/produit/Detailsproduit.vue';
import Search from '@/views/search/SearchList.vue';
import Messages from '@/views/utilisateur/Messages.vue';
import Commandes from '@/views/utilisateur/Commandes.vue';
import ListingAbo from '@/views/utilisateur/Abonnement.vue';



const routes = [
    {
        name: 'Home',
        path: '/',
        component: Home,
    },{
        name: 'ListingProduit',
        path: '/produits',
        component: Listingproduit,
    }, {
        name: 'ListingProduitCat',
        path: '/produits/:cat',
        component: ListingproduitCat,
    },{
        name: 'Profil',
        path: '/profil',
        component: Profil,
    }, {
        name: 'Inscription',
        path: '/inscription',
        component: Inscription,
    }, {
        name: 'GestionProduit',
        path: '/profilproduits',
        component: Produits,
    }, {
        name: 'Connexion',
        path: '/connexion',
        component: Connexion,
    }, {
        name: 'Detailsproduit',
        path: '/:idVendeur/:id',
        component: Detailsproduit,
    }, {
        name: 'Editproduit',
        path: '/edit/:idVendeur/:id',
        component: Editproduit,
    }, {
        name: 'Profilvendeur',
        path: '/:idVendeur/',
        component: Profilvendeur,
    }, {
        name: 'AllMessages',
        path: '/allmessages',
        component: Messages,
    },{
        name: 'Messages',
        path: '/messages/:destinataireid/',
        component: Messages,
    }, {
        name: 'Search',
        path: '/rechercher/:val/',
        component: Search,
    }, {
        name: 'ListingAbo',
        path: '/mes-abonnements/',
        component: ListingAbo,
    }, {
        name: 'Commandes',
        path: '/mes-commandes/',
        component: Commandes,
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

// router.beforeEach(async (to, from) => {
//     if (
//       // make sure the user is authenticated
//       !isAuthenticated &&
//       // ❗️ Avoid an infinite redirect
//       to.name !== 'Connexion'
//     ) {
//       // redirect the user to the Connexion page
//       return { name: 'Connexion' }
//     }
//   })

export default router;