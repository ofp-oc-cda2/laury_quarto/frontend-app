import { defineStore } from 'pinia';
import * as jose from 'jose';
import axios from 'axios';


// useStore could be anything like useUser, useCart
// the first argument is a unique id of the store across your application
export const useStore = defineStore('main', {

  state: () => {
    let token = localStorage.getItem('token');

    //décode token
    if(token != '' && token != null){
      const payload = jose.decodeJwt(token);

      return {
        user: null,
        token: token,
        id: payload.id,
        username: payload.username,
        seller: null
      }

    } else {
      
      return {
        user: null,
        token: '',
        username: '',
        seller: null
      }
    }
  },

  getters: {
    isConnected(state){
      return state.token != null && state.token != '' && state.user != null;
    }
  },

  actions: {
    login(token) {
      this.token = token;
      window.localStorage.setItem('token', token);
      //décode token
      const payload = jose.decodeJwt(token)
      this.username = payload.username;
      this.fetchUser();
    },

    fetchUser(){
      const url = process.env.VUE_APP_URL;
      return axios({
        method: 'get',
        url: url + "/api/utilisateurs/me",
        headers: {
          Authorization: `Bearer ${this.token}`
        }
      })
      .then(res => {
        this.user = res.data;
      })
      .catch(err => {
          console.log(err);
      })
    },

    fetchSeller(idUser){
      const url = process.env.VUE_APP_URL;
      return axios.get(url + "/api/info_vendeurs/getbyid/" + idUser)
      .then(res => {
          if(res.data['hydra:member'].length > 0){
              this.seller = res.data['hydra:member'][0];
          };
      })
      .catch(err => {
          console.log(err)
      })
    },

    logout() {
      this.token = null;
      localStorage.removeItem('token');
    }
  }
})